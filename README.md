We are a multi award winning salon, situated in the heart of Norwich.
We are known for our outstanding customer service, we make sure your precious time spent with us has the focus solely on you.
We offer an extensive range of beauty, nail & relaxation treatments of the highest quality. || Address: 10 Farmers Ave, Norwich NR1 3JX, United Kingdom || Phone: +44 1603 611223 || Website: https://www.charismanorwich.co.uk
